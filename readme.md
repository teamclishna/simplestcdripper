# Simplest CD Ripper

## What's this?

This is a command prompt application for ripping audio CDs.
The CD ripping process uses the "CSAudioCDRipper" library.
This library was developed by Microncode.

https://microncode.com/developers/cs-audio-cd-ripper/

## System Requirements

* Windows 10
* .net Framework 4.8

## How to install.

This application does not use installer.
Copy SimplestCDRipper.exe and files on same folder copy to any folder.

## How to uninstall.

This application dos not use uninstaller.
If you will not use this application.
Remove directory and files that you copied when install.

## How to Use
The simplest usage is as follows.
```
SimplestCDRipper.exe
```

* Of the CD drives connected to your PC, the first one found will be used for ripping.
* Rip all tracks on the inserted audio CD.
* The ripped file name is "Track xx.mp3". (xx is the track number)
* The ripped file is output to the current directory.
* The encoding is mp3 and the bit rate is 320kbps.

To change the bit rate, add the following switch.
```
SimplestCDRipper.exe --bitrate 128
```
CSAudioCDRipper supports many bitrates,
but in general it is sufficient to specify one of 64, 128, 192, 320.

To specify the output destination directory, add the following switch.
```
SimplestCDRipper.exe ^
  --destination "D:\MyMusic\MyAlbum"
```
NOTE: The "^" symbol is a symbol that continues typing on the next line at the command prompt.

If this parameter is specified, an error will occur if the specified directory does not exist.
If you want to create the specified directory automatically, add the following switch.
```
SimplestCDRipper.exe ^
  --destination "D:\MyMusic\MyAlbum" --mkdir
```
Even if the mkdir switch is specified,
an error will occur if the directory above the lowest level directory does not exist.
("MyMusic" directory in the above example)

To specify the track to be ripped, add the following switch.
When ripping multiple tracks, add as many switches as there are target tracks.

```
SimplestCDRipper.exe ^
  --destination "D:\MyMusic\MyAlbum" ^
  --track 1 ^
  --track 3 ^
  --track 4
```
This example rips tracks 1, 3 and 4.

If you want to give a song title to the ID3 tag, add the following switch.
```
SimplestCDRipper.exe ^
  --destination "D:\MyMusic\MyAlbum" ^
  --track 1 --title "The Bast Music" ^
  --track 3 --title "My Favorite song" ^
  --track 4 --title "Microsoft Windows 2020"
```
You must specify as many title switches as there are track switches.
An error will occur if the number of switches is different.

If you want to register the album name and composer name in the ID3 tag,
add the following switch.
```
SimplestCDRipper.exe ^
  --destination "D:\MyMusic\MyAlbum" ^
  --albumname "Sunlight yellow overdrive" ^
  --artist "Joseph Joestar" ^
  --track 1 --title "The Bast Music" ^
  --track 3 --title "My Favorite song" ^
  --track 4 --title "Microsoft Windows 2020"
```
The album name and artist name should be the same for all tracks.

If you want to change the audio file format to something other than mp3,
add the following switch.
```
SimplestCDRipper.exe --format ogg
```

Album art can be set in the output file.
Images that can be set for album art are JPEG (extension .jpg or .jpeg) and PNG (extension .png).
Applies only when outputting MP3 format files.

The albumart switch can only be specified once.
When multiple tracks are set as ripping targets, the specified image is set for all of them.
It is not possible to set different images for each track.

for example:
```
SimplestCDRipper.exe ^
  --test ^
  --destination "D:\Data\Documents\temp\out\mycd" ^
  --mkdir ^
  --albumname "Sunlight yellow overdrive" ^
  --artist "Joseph Joestar" ^
  --albumart "D:\Data\Documents\temp\overdrive.jpg" ^
  --track 1 --title "The Bast Music" ^
  --track 3 --title "My Favorite song" ^
  --track 4 --title "Microsoft Windows 2020"
```

Although CDAudioCDRipper supports many output formats.
I have confirmed the operation of only mp3, wma and ogg.

If you add the --test switch as shown below,
you can check how the ripping process is performed according to the specified parameters.

In this case, the actual ripping process is not performed.

for example:
```
SimplestCDRipper.exe ^
  --test ^
  --destination "D:\Data\Documents\temp\out\mycd" ^
  --mkdir ^
  --albumname "Sunlight yellow overdrive" ^
  --artist "Joseph Joestar" ^
  --track 1 --title "The Bast Music" ^
  --track 3 --title "My Favorite song" ^
  --track 4 --title "Microsoft Windows 2020"
```
output example:
```
CD drive : E:\ TSSTcorp BDDVDW SN-506AB  AF01
Output format: MP3
Sampling rate: esamples44100
Bit rate: bitrate320
Destination directory: D:\Data\Documents\temp\out\mycd
(Create Directory)
---- Tracks ----
#1      D:\Data\Documents\temp\out\mycd\01 The Bast Music.mp3   Album="Sunlight yellow overdrive"       Title="The Bast Music"  Artist="Joseph Joestar"
#3      D:\Data\Documents\temp\out\mycd\03 My Favorite song.mp3 Album="Sunlight yellow overdrive"       Title="My Favorite song"        Artist="Joseph Joestar"
#4      D:\Data\Documents\temp\out\mycd\04 Microsoft Windows 2020.mp3   Album="Sunlight yellow overdrive"       Title="Microsoft Windows 2020"  Artist="Joseph Joestar"
```

In addition, the implemented option switch is displayed by the following method.
```
SimplestCDRipper.exe --help
```

## FAQ
The following message is displayed at the start of ripping.

```
****************************************************************
The CSAudioCDRipper runs in a FREE mode for personal or FREE use.
For commercial or any other use please order a license at
https://www.microncode.com/developers/cs-audio-cd-ripper/
****************************************************************

CSCore.MediaFoundation.MFMediaType
```

This message is displayed when you are using CSAudioCDRipper for free.
If you want to clear this message, please follow the steps below to modify
the application **yourself**.

1. Contact microncode to purchase a license and receive a license key.
2. Download the source code for this application from Bitbuclket.
3. Set user name and license key to source code "Ripper/RipperMain.cs".
4. Recompile by including the license key you received in your application.

## Update History.

### ver. 1.1.0

* Added support for setting artwork images when ripping in MP3 format.
* Added support for setting ID3 tags for genre, recording year, comments, and copyright notice.

## License.

### This Application:
You can use this application for free.

This application has released the source code.
You can download the source code and fix bugs or add functions.

This application is **not** open source software.

You cannot distribute or reprint this application.
When you distribute the source code that you modified,
you can distribute only the modified part.

### CSAudioCDRipper:
CSAudioCDRipper can only be used free of charge for free applications.
In addition, the source code of this library is MS-PL licensed.

https://www.nuget.org/packages/CSAudioCDRipper/1.1.4/license

and see:

CSAudioCDRipper.License.txt

## Author

Team ClishnA

* [InfoClishnA](http://clishna.info/download/simplestcdripper/)
* [Source code](https://bitbucket.org/teamclishna/simplestcdripper/)
