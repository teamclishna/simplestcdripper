﻿using CSAudioCDRipper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace SimplestCDRipper
{
    /// <summary>
    /// コマンドラインで指定したリッピング時のパラメータを格納するクラス
    /// </summary>
    public class RipperConfig
    {
        /// <summary>
        /// PC に接続されている光学ドライブの情報.
        /// "X: <ドライブのデバイス名>"
        /// の書式で取得し、列挙する
        /// </summary>
        public List<string> Drives { get; set; }

        /// <summary>リッピングに使用するデバイス</summary>
        public string TargetDrive { get; set; }

        /// <summary>リッピングしたファイルを出力するディレクトリ</summary>
        public DirectoryInfo DestinationDir { get; set; }

        /// <summary>DestinationDir が存在しないディレクトリの場合、このディレクトリの作成を許可するかどうか</summary>
        public bool AllowCreateDirectory { get; set; }

        /// <summary>リッピング対象の情報を出力し、実際のリッピングは行わない</summary>
        public bool TestRippingPlan { get; set; }

        /// <summary>リッピングするファイル型式</summary>
        public Format FileFormat { get; set; }
        /// <summary>サンプリングレート</summary>
        public Samplerate SampleRate { get; set; }
        /// <summary>ビットレート</summary>
        public Bitrate Bitrate { get; set; }

        /// <summary>ID3タグに着けるアルバム名</summary>
        public string AlbumName { get; set; }
        /// <summary>アルバムアートに設定する画像ファイルのパス</summary>
        public string AlbumArtPath { get; set; }
        /// <summary>ID3タグに着けるトラック番号. 省略時は全トラックをリッピングの対象とする</summary>
        public List<int> Tracks { get; private set; }
        /// <summary>ID3タグに着けるアーティスト名. 非省略時はTracksと同じ要素数にしなければならない</summary>
        public List<string> Artists { get; private set; }
        /// <summary>ID3タグに着けるタイトル. 非省略時はTracksと同じ要素数にしなければならない</summary>
        public List<string> Titles { get; private set; }
        /// <summary>出力するメディアファイルのファイル名. 拡張子、ディレクトリ名は含めない. 非省略時はTracksと同じ要素数にしなければならない</summary>
        public List<string> FileNames { get; private set; }
        /// <summary>出力するメディアファイルのジャンル. 非省略時はTracksと同じ要素数にしなければならない</summary>
        public List<string> Genres { get; private set; }
        /// <summary>
        /// 出力するメディアファイルの収録年. ID3タグの仕様としては「4桁の数値」であるが、本アプリでは [1900, 2099] の範囲のみ認める.
        /// 非省略時はTracksと同じ要素数にしなければならない</summary>
        public List<int> Years { get; private set; }
        /// <summary>出力するメディアファイルの著作権表記. 非省略時はTracksと同じ要素数にしなければならない</summary>
        public List<string> Copyrights { get; private set; }
        /// <summary>出力するメディアファイルのコメント. 非省略時はTracksと同じ要素数にしなければならない</summary>
        public List<string> Comments { get; private set; }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public RipperConfig()
        {
            this.Drives = new List<string>();
            this.Tracks = new List<int>();
            this.Artists = new List<string>();
            this.Titles = new List<string>();
            this.FileNames = new List<string>();
            this.Genres = new List<string>();
            this.Years = new List<int>();
            this.Copyrights = new List<string>();
            this.Comments = new List<string>();
        }
    }
}
