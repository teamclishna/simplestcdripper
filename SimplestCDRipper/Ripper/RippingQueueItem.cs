﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace SimplestCDRipper.Ripper
{
    /// <summary>
    /// リッピング対象のトラック情報
    /// </summary>
    public class RippingQueueItem
    {
        public int TrackNumber { get; set; }
        public string AlbumName { get; set; }
        public string Title { get; set; }
        public string ArtistName { get; set; }
        public string Genre { get; set; }
        public string Copyright { get; set; }
        public string Comment { get; set; }
        public int Year { get; set; }
        public FileInfo FilePath { get; set; }

        public override string ToString()
        {
            return $"#{this.TrackNumber}\t" +
                $"{this.FilePath}\t" +
                $"Album=\"{this.AlbumName}\"\t" +
                $"Title=\"{this.Title}\"\t" +
                $"Artist=\"{this.ArtistName}\"\t" +
                $"Copyright=\"{this.Copyright}\"\t" +
                $"Year=\"{this.Year}\"\t" +
                $"Comment=\"{this.Comment}\"";
        }
    }
}
