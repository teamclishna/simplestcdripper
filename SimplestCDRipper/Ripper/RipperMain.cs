﻿using clUtils.log;
using CSAudioCDRipper;
using Microsoft.Win32;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SimplestCDRipper.Ripper
{
    /// <summary>
    /// CDリッパーのメインクラス
    /// </summary>
    public class RipperMain
    {
        // TODO: CSAudioCDRipper ライブラリのライセンスキーと購入者情報を記入する. 未購入の場合は null のままにしておく.
        // (この場合、リッピング開始時に無償使用中である旨のメッセージが表示される。
        
        /// <summary>CSAudioCDRipper購入時のユーザ名</summary>
        private static readonly string LICENSE_USERNAME = null;
        /// <summary>CSAudioCDRipper購入時に発行されたライセンスキー</summary>
        private static readonly string LICENSE_KEY = null;

        #region Properties

        /// <summary>ロガーオブジェクト</summary>
        public ILogger Logger { get; set; }

        /// <summary>コマンドライン引数で指定したリッピング対象の情報</summary>
        public RipperConfig Conf { get; set; }

        /// <summary>リッピングに使用するCDリッパー</summary>
        private AudioCDRipper Ripper { get; set; }

        /// <summary>リッピング対象のトラック情報. リストの先頭要素から順にリッピングする</summary>
        public List<RippingQueueItem> Queue { get; set; }

        /// <summary>リッピング処理の完了したトラック情報</summary>
        public RippingQueueItem DoneQueue { get; set; }
        
        /// <summary>リッピング中にエラーが発生した場合、ここにスローされた例外を格納する</summary>
        public Exception RippingException { get; set; }

        /// <summary>リッピングの進行状況を出力するためのTextWriter</summary>
        private TextWriter RippingProgressWriter { get; set; }

        #endregion

        #region Event Handlers

        private delegate void RippingEndedHandler(object sender, EventHandler args);

        private RippingEndedHandler OnRippingEnded;

        #endregion


        /// <summary>
        /// 初期化処理.
        /// PCに接続されているデバイスの情報を取得して設定情報に格納する
        /// </summary>
        /// <returns></returns>
        public RipperMain Init()
        {
            this.Ripper = new CSAudioCDRipper.AudioCDRipper()
            {
                UserName = LICENSE_USERNAME,
                UserKey = LICENSE_KEY,
            };
            this.Conf.Drives = this.Ripper.GetDevices();

            return this;
        }

        /// <summary>
        /// コマンドライン引数でドライブレターを指定した光学ドライブのインデックスを取得する
        /// </summary>
        /// <returns>対応するインデックス. 対応するドライブレターが存在しないバイアは負の値を返す</returns>
        public int GetDeviceIndex()
        {
            // デバイスが1つもささってない場合は終了
            if (this.Conf.Drives.Count == 0)
            { return -1; }

            // デバイスが指定されていない場合は最初に見つかったものを返す
            if (string.IsNullOrEmpty(this.Conf.TargetDrive))
            { return 0; }

            var driveletter = this.Conf.TargetDrive.ToLower();
            var index = 0;
            foreach (var driveName in this.Conf.Drives)
            {
                if (driveName.ToLower().StartsWith(driveletter))
                { return index; }
                index++;
            }

            return -1;
        }

        /// <summary>
        /// PCに接続されている光学ドライブを列挙して指定したストリームに出力する
        /// </summary>
        /// <param name="writer">出力対象のストリーム</param>
        public void EnumDevices(TextWriter writer)
        {
            foreach (var drive in this.Conf.Drives)
            { writer.WriteLine(drive); }
        }

        /// <summary>
        /// リッパーがサポートしているエンコーディングを列挙して指定したストリームに出力する
        /// </summary>
        /// <param name="writer">出力対象のストリーム</param>
        public void EnumFormats(TextWriter writer)
        {
            foreach (var fmt in this.Ripper.GetFormats())
            { writer.WriteLine(fmt); }
        }

        /// <summary>
        /// CDのトレイを開く
        /// </summary>
        public void OpenCDDoor()
        {
            // デバイスが使用可能？
            var deviceIndex = GetDeviceIndex();
            if (deviceIndex < 0 || !this.Ripper.CDIsReady(deviceIndex))
            { throw new InvalidOperationException("Device Not Ready"); }

            this.Ripper.EjectCD(deviceIndex);
        }

        /// <summary>
        /// CDのトレイを閉じる
        /// </summary>
        public void CloseCDDoor()
        {
            // デバイスが使用可能？
            var deviceIndex = GetDeviceIndex();
            if (deviceIndex < 0 || !this.Ripper.CDIsReady(deviceIndex))
            { throw new InvalidOperationException("Device Not Ready"); }

            this.Ripper.CloseCD(deviceIndex);
        }

        /// <summary>
        /// 光学ドライブに挿入されているCDのトラック情報を指定したストリームに出力する
        /// </summary>
        /// <param name="writer">出力対象のストリーム</param>
        public void EnumTrackInfo(TextWriter writer)
        {
            var deviceIndex = GetDeviceIndex();
            if (deviceIndex < 0 || !this.Ripper.CDIsReady(deviceIndex))
            {
                writer.WriteLine("[ERROR] Device not ready.");
                return;
            }

            foreach (var trk in this.Ripper.GetTracks(deviceIndex))
            {
                var trackTime = trk.TrackLengthTime.ToString(@"mm\:ss\.fff");
                var line = $"#{trk.TrackNumber}, TrackType = \"{trk.TrackType}\", TrackLengthTime = \"{trackTime}\"";

                writer.WriteLine(line);
            }
        }

        /// <summary>
        /// リッピングを開始する
        /// </summary>
        /// <param name="writer"></param>
        public void Start(TextWriter writer)
        {
            this.Queue = new List<RippingQueueItem>();

            // リッピング対象のトラックを取得
            var tracks = GetRippingTrack();

            // トラック情報に曲名、ファイル名などの情報を付与する
            SetTrackNameInfo(ref tracks);

            if (this.Conf.TestRippingPlan)
            {
                // リッピング対象の情報を表示
                ShowRippingPlan(writer, tracks);
            }
            else
            {
                // リッピング開始
                StartRip(writer, tracks);
            }
        }

        /// <summary>
        /// リッピング設定を表示する.
        /// 実際のリッピングは行わない
        /// </summary>
        /// <param name="writer">出力対象のTextWriter</param>
        /// <param name="tracks">リッピング対象のトラック情報</param>
        private void ShowRippingPlan(
            TextWriter writer,
            List<RippingQueueItem> tracks)
        {
            // 出力先デバイス
            var deviceName = this.Ripper.GetDevices()[GetDeviceIndex()];
            writer.WriteLine($"CD drive : {deviceName}");

            // 出力フォーマット、サンプリングレートなど
            writer.WriteLine($"Output format: {this.Conf.FileFormat}");
            writer.WriteLine($"Sampling rate: {this.Conf.SampleRate}");
            writer.WriteLine($"Bit rate: {this.Conf.Bitrate}");

            // リッピング先のディレクトリを作成するかどうか？
            var destDir = GetOutputDirectory();
            if (destDir.Exists)
            {
                writer.WriteLine($"Destination directory: {destDir.FullName}");
            }
            else
            {
                if (this.Conf.AllowCreateDirectory)
                {
                    writer.WriteLine($"Destination directory: {destDir.FullName}");
                    writer.WriteLine($"(Create Directory)");
                }
                else
                {
                    writer.WriteLine($"[ERROR] Directory {destDir.FullName} not found and not allow create directory.");
                }
            }

            // トラック情報
            writer.WriteLine("---- Tracks ----");
            foreach (var tr in tracks)
            { writer.WriteLine(tr.ToString()); }
        }

        /// <summary>
        /// リッピング処理を開始する
        /// </summary>
        /// <param name="writer">リッピングの進行状況を出力するTextWriter</param>
        /// <param name="tracks">リッピング対象のトラック情報</param>
        private void StartRip(
            TextWriter writer,
            List<RippingQueueItem> tracks)
        {
            // リッピングしたファイルの出力先ディレクトリを作成する
            MkDir(GetOutputDirectory());

            // リッピング開始
            this.RippingProgressWriter = writer;
            this.Queue = tracks;
            InitRipper();

            // キューがなくなるまでリッピングを続ける
            // また、次の曲のリッピングを開始可能かどうかを
            // OperationState プロパティで確認しながら待機する
            while (0 < this.Queue.Count())
            {
                if (this.Ripper.OperationState == CSAudioCDRipper.OperationState.Running)
                {
                    // リッピング実行中
                }
                else
                {
                    if (this.DoneQueue != null)
                    {
                        // リッピング完了したらID3タグの書き込み処理を行う
                        SaveID3Tags(this.DoneQueue);

                        // キューから書き込み完了したトラックの情報を削除し、次の曲のリッピングを開始
                        this.Queue.Remove(this.DoneQueue);
                        this.RippingException = null;
                        this.DoneQueue = null;
                    }

                    if (0 < this.Queue.Count())
                    { StartRipTrack(this.Queue[0]); }
                }
                Thread.Sleep(1000);
            }

            if (this.RippingException != null)
            { throw this.RippingException; }

            writer.WriteLine("All tracks done.");
        }

        /// <summary>
        /// 指定したトラックのリッピングを開始する
        /// </summary>
        /// <param name="queue"></param>
        private void StartRipTrack(RippingQueueItem queue)
        {
            // リッピング対象のトラック情報を設定する
            // キュー情報では1から始まるトラック番号を指定するが
            // SourceTrackプロパティは0を先頭としているので1引いた値を設定する
            this.Ripper.DestinatioFile = queue.FilePath.FullName;
            this.Ripper.SourceTracks.Clear();
            this.Ripper.SourceTracks.Add(new Options.Core.SourceTrack(queue.TrackNumber - 1));

            this.Ripper.Rip();
        }

        /// <summary>
        /// 指定したキューのファイルに対してID3タグの書き込みを行う
        /// </summary>
        /// <param name="queue"></param>
        private void SaveID3Tags(RippingQueueItem queue)
        {
            try
            {
                // 曲名情報を設定できないフォーマット形式の場合は除外する
                if ((this.Conf.FileFormat != Format.WAV) &&
                    (this.Conf.FileFormat != Format.ACM) &&
                    (this.Conf.FileFormat != Format.AAC))
                {
                    this.Ripper.TagAlbum = queue.AlbumName;
                    this.Ripper.TagArtists = new List<string>() { queue.ArtistName };
                    this.Ripper.TagTitle = queue.Title;
                    this.Ripper.TagTrack = (uint)queue.TrackNumber;
                    this.Ripper.TagGenres = new List<string>() { queue.Genre };
                    this.Ripper.TagCopyright= queue.Copyright;
                    this.Ripper.TagYear = (0 < queue.Year ? queue.Year.ToString() : null);
                    // コメントは省略すると謎の16進表記が挿入されるので
                    // 無視体の場合は空白を明示的に入れておく
                    this.Ripper.TagComment = (string.IsNullOrEmpty(queue.Comment) ? " " : queue.Comment);

                    // アートワークを書き込む
                    // アートワークは mp3 の場合のみ書き込むことにする
                    if (this.Conf.FileFormat == Format.MP3 &&
                        this.Conf.AlbumArtPath != null)
                    {
                        this.Ripper.AddImageFromFile(
                            this.Conf.AlbumArtPath,
                            "Front Cover Artwork",
                            CSID3TagsLib.ImageType.FrontCover);
                    }
                    this.Ripper.SetID3Tags(queue.FilePath.FullName);
                }

                this.RippingProgressWriter.WriteLine($"\r{queue.FilePath.Name}: ID3Tag written.");
            }
            catch (Exception eUnknown)
            {
                this.RippingProgressWriter.WriteLine($"\r{queue.FilePath.Name}: ID3Tag write Failed.");
                this.RippingProgressWriter.WriteLine(eUnknown.Message);
            }
        }

        /// <summary>
        /// リッピング対象のトラック情報を取得する
        /// </summary>
        /// <returns></returns>
        private List<RippingQueueItem> GetRippingTrack()
        {
            // デバイスが使用可能？
            var deviceIndex = GetDeviceIndex();
            if (deviceIndex < 0 || !this.Ripper.CDIsReady(deviceIndex))
            { throw new InvalidOperationException("Device Not Ready"); }

            // CDのトラック情報を取得する
            var tracks = this.Ripper.GetTracks(deviceIndex);
            if(this.Conf.Tracks.Count == 0)
            {
                // リッピング対象のトラックが指定されていない場合は全トラックが対象
                return tracks.Select(tr => new RippingQueueItem()
                { TrackNumber = tr.TrackNumber, }
                ).ToList();
            }
            else
            {
                // 指定したトラックだけリッピング対象にする
                // この時、同じトラック数が複数回現れたり範囲外のトラックが指定された場合はエラー
                var result = new List<RippingQueueItem>();
                foreach(var target in this.Conf.Tracks)
                {
                    if (tracks.Length < target)
                    { throw new IndexOutOfRangeException($"Track Number {target} is Out of bound. (1 - {tracks.Length})"); }

                    if (result.Any(tr => tr.TrackNumber == target))
                    { throw new InvalidOperationException($"Track Number {target} specified multiple times."); }

                    result.Add(new RippingQueueItem() { TrackNumber = target, });
                }
                return result;
            }
        }

        /// <summary>
        /// リッピング対象のトラック情報にファイル名、曲名、アーティスト情報を設定する
        /// </summary>
        /// <param name="queue"></param>
        private void SetTrackNameInfo(ref List<RippingQueueItem> queue)
        {
            // 曲名、ファイル名が設定されている場合、リッピング対象の曲数と一致しない場合はエラー
            if (0 < this.Conf.Titles.Count && this.Conf.Titles.Count != queue.Count)
            { throw new InvalidOperationException($"Number of titles does not match the number of songs to be ripped."); }

            if (0 < this.Conf.FileNames.Count && this.Conf.FileNames.Count != queue.Count)
            { throw new InvalidOperationException($"Number of file names does not match the number of songs to be ripped."); }

            // ・アーティスト名
            // ・ジャンル
            // ・著作権表記
            // ・収録年
            // ・コメント
            // は省略、1件、またはリッピング対象の曲数と一致しない場合はエラー
            if (1 < this.Conf.Artists.Count && this.Conf.Artists.Count != queue.Count)
            { throw new InvalidOperationException($"Number of artist names does not match the number of songs to be ripped."); }
            if (1 < this.Conf.Genres.Count && this.Conf.Genres.Count != queue.Count)
            { throw new InvalidOperationException($"Number of genres does not match the number of songs to be ripped."); }
            if (1 < this.Conf.Copyrights.Count && this.Conf.Copyrights.Count != queue.Count)
            { throw new InvalidOperationException($"Number of copyright does not match the number of songs to be ripped."); }
            if (1 < this.Conf.Years.Count && this.Conf.Years.Count != queue.Count)
            { throw new InvalidOperationException($"Number of recording years does not match the number of songs to be ripped."); }
            if (1 < this.Conf.Comments.Count && this.Conf.Comments.Count != queue.Count)
            { throw new InvalidOperationException($"Number of comments does not match the number of songs to be ripped."); }

            // アルバム名を設定
            if (!string.IsNullOrEmpty(this.Conf.AlbumName))
            {
                for (var index = 0; index < queue.Count; index++)
                { queue[index].AlbumName = this.Conf.AlbumName; }
            }

            // 曲名を設定
            for (var index = 0; index < queue.Count; index++)
            { queue[index].Title = (0 < this.Conf.Titles.Count ? this.Conf.Titles[index] : $"Track {queue[index].TrackNumber:00}"); }

            // 1件、または個別設定可能な項目を設定
            // デフォルトでは空文字を設定する
            // 1件だけ設定されている場合は全曲に同じ内容を設定する
            for (var index = 0; index < queue.Count; index++)
            {
                queue[index].ArtistName = (
                    0 == this.Conf.Artists.Count ? "" :
                    1 == this.Conf.Artists.Count ? this.Conf.Artists[0] :
                    this.Conf.Artists[index]);
                queue[index].Genre = (
                    0 == this.Conf.Genres.Count ? "" :
                    1 == this.Conf.Genres.Count ? this.Conf.Genres[0] :
                    this.Conf.Genres[index]);
                queue[index].Copyright = (
                    0 == this.Conf.Copyrights.Count ? "" :
                    1 == this.Conf.Copyrights.Count ? this.Conf.Copyrights[0] :
                    this.Conf.Copyrights[index]);
                queue[index].Year = (
                    0 == this.Conf.Years.Count ? 0 :
                    1 == this.Conf.Years.Count ? this.Conf.Years[0] :
                    this.Conf.Years[index]);
                queue[index].Comment = (
                    0 == this.Conf.Comments.Count ? "" :
                    1 == this.Conf.Comments.Count ? this.Conf.Comments[0] :
                    this.Conf.Comments[index]);
            }

            // ファイル名を設定
            // 出力先ディレクトリを含めたフルパスを設定する
            // ファイル名は
            // 1. 明示的に設定されていればそのファイル名
            // 2. タイトルが設定されていればトラック番号 + タイトル
            // 3. 未指定の場合は "Track 00"
            // の優先順でつける
            for (var index = 0; index < queue.Count; index++)
            {
                var fileName = (
                    0 < this.Conf.FileNames.Count ? this.Conf.FileNames[index] :
                    0 < this.Conf.Titles.Count ? $"{queue[index].TrackNumber:00} {this.Conf.Titles[index]}" :
                    $"Track {queue[index].TrackNumber:00}"
                    ) + 
                    "." + this.Conf.FileFormat.ToString().ToLower();

                queue[index].FilePath = new FileInfo(Path.Combine(GetOutputDirectory().FullName, fileName));
            }
        }

        /// <summary>
        /// リッピングしたファイルの出力先ディレクトリを作成する
        /// </summary>
        /// <returns>
        /// 設定で出力先が明示されている場合はそのディレクトリ.
        /// 未設定の場合はカレントディレクトリ
        /// </returns>
        private DirectoryInfo GetOutputDirectory()
        {
            return (
                this.Conf.DestinationDir != null ? this.Conf.DestinationDir :
                new DirectoryInfo(Environment.CurrentDirectory));
        }

        /// <summary>
        /// 指定したディレクトリを作成する.
        /// </summary>
        /// <param name="output">作成するディレクトリ.</param>
        /// <exception cref="InvalidOperationException">
        /// 指定したディレクトリが存在せず、かつ設定でディレクトリの作成を許可していない場合
        /// </exception>
        private void MkDir(DirectoryInfo output)
        {
            if (!output.Exists)
            {
                if (this.Conf.AllowCreateDirectory)
                { output.Create(); }
                else
                { throw new InvalidOperationException($"Directory {output.FullName} not found and not allow create directory."); }
            }
        }

        /// <summary>
        /// リッピングのイベントハンドラを設定する
        /// </summary>
        private void InitRipper()
        {
            this.Ripper.RipProgress += new CSAudioCDRipper.AudioCDRipper.RipProgressEventHandler(Ripper_RipProgress);
            this.Ripper.RipError += new CSAudioCDRipper.AudioCDRipper.RipErrorEventHandler(Ripper_RipError);
            this.Ripper.RipStart += new CSAudioCDRipper.AudioCDRipper.RipStartEventHandler(Ripper_RipStart);
            this.Ripper.RipDone += new CSAudioCDRipper.AudioCDRipper.RipDoneEventHandler(Ripper_RipDone);

            // リッピングパラメータを設定する
            this.Ripper.SelectedDriveIndex = GetDeviceIndex();
            this.Ripper.Format = this.Conf.FileFormat;
            this.Ripper.Samplerate = this.Conf.SampleRate;
            this.Ripper.Bitrate = this.Conf.Bitrate;
            this.Ripper.Bits = Bits.bits32;
            this.Ripper.Channels = Channels.channels2;

            this.DoneQueue = null;
            this.RippingException = null;
        }

        #region Event handler for cd ripper

        /// <summary>
        /// リッピング開始時に呼び出されるイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Ripper_RipStart(object sender, EventArgs e)
        {
            // var r = (AudioCDRipper)sender;
            // var fileName = new FileInfo(r.DestinatioFile).Name;
            // this.RippingProgressWriter.WriteLine($"\r{fileName} Start");
        }

        /// <summary>
        /// リッピングの処理進行中に呼び出されるイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Ripper_RipProgress(object sender, PercentArgs e)
        {
            var r = (AudioCDRipper)sender;
            var fileName = new FileInfo(r.DestinatioFile).Name;
            this.RippingProgressWriter.Write($"\r{fileName}: {e.Number}%");
        }

        /// <summary>
        /// 1曲のリッピングが完了した時に呼び出されるイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Ripper_RipDone(object sender, EventArgs e)
        {
            if (this.RippingException != null)
            {
                // リッピングに失敗した場合は何もせずに終了する
            }
            else
            {
                // リッピング完了したファイルに対応するキューを検索し、
                // これを処理完了済みアイテムとして登録する
                // 処理済みアイテムに既に同じファイルの情報が入っている場合は無視する
                // (理由がよくわからないが RipDone がトラック毎に2回ずつ呼び出されているため)
                var r = (CSAudioCDRipper.AudioCDRipper)sender;
                if (this.DoneQueue != null && this.DoneQueue.FilePath.FullName == r.DestinatioFile)
                { }
                else
                {
                    var fileName = new FileInfo(r.DestinatioFile).Name;
                    this.RippingProgressWriter.WriteLine($"\r{fileName}: Done.");
                    if (0 < this.Queue.Count)
                    {
                        var done = this.Queue.FirstOrDefault(x => x.FilePath.FullName.Equals(r.DestinatioFile));
                        if (done != null)
                        { this.DoneQueue = done; }
                    }
                }
            }
        }

        /// <summary>
        /// リッピング失敗時に呼び出されるイベント.
        /// このイベントが呼び出された時点で残っている曲のリッピングも中断する
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Ripper_RipError(object sender, MessageArgs e)
        {
            var r = (CSAudioCDRipper.AudioCDRipper)sender;
            var fileName = new FileInfo(r.DestinatioFile).Name;
            this.RippingProgressWriter.Write($"\r{fileName}: Failed.");
            this.RippingProgressWriter.WriteLine();

            this.RippingException = new Exception($"{e.Number:x08}\t{e.String}");

            // 残りのキューをすべて破棄
            this.Queue.Clear();
        }

        #endregion

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public RipperMain()
        { }
    }
}
