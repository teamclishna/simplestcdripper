﻿using CSAudioCDRipper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using clUtils.log;
using clUtils.util;
using SimplestCDRipper.Ripper;
using System.Reflection;

namespace SimplestCDRipper
{
    public class Program
    {
        /// <summary>コマンドラインヘルプを記述したテキストファイルのリソース名</summary>
        private static readonly string USAGE_FILE_NAME = "SimplestCDRipper.Resources.usage.txt";

        private static CommandLineOptionDefinition[] CMD_ARGS =
        {
            // コマンドラインスイッチのヘルプ
            new CommandLineOptionDefinition(){ LongSwitch = "help",        ShortSwitch = 'h', RequireParam = false, AllowMultiple = false, },
            new CommandLineOptionDefinition(){ LongSwitch = "usage",       ShortSwitch = '?', RequireParam = false, AllowMultiple = false, },
            // PCに接続されている光学ドライブのデバイス名を列挙する
            new CommandLineOptionDefinition(){ LongSwitch = "devices",                        RequireParam = false, AllowMultiple = false, },
            // リッパーがサポートしているエンコーディング名を列挙する
            new CommandLineOptionDefinition(){ LongSwitch = "formats",                        RequireParam = false, AllowMultiple = false, },
            // CDドライブのトレイを開く
            new CommandLineOptionDefinition(){ LongSwitch = "open",                           RequireParam = false, AllowMultiple = false, },
            // CDドライブのトレイを閉じる
            new CommandLineOptionDefinition(){ LongSwitch = "close",                          RequireParam = false, AllowMultiple = false, },
            // リッピング対象のトラックと出力ファイル名の情報を出力する
            // 実際にリッピングは行わない
            new CommandLineOptionDefinition(){ LongSwitch = "test",                           RequireParam = false, AllowMultiple = false, },
            // リッピングの対象とする光学ドライブのドライブレターを指定する (コロンは含まない)
            new CommandLineOptionDefinition(){ LongSwitch = "drive",       ShortSwitch = 'd', RequireParam = true,  AllowMultiple = false, },
            // 挿入されているCDのトラック情報を取得
            new CommandLineOptionDefinition(){ LongSwitch = "info",        ShortSwitch = 'i', RequireParam = false, AllowMultiple = false, },
            // リッピングしたファイルの出力先ディレクトリ
            new CommandLineOptionDefinition(){ LongSwitch = "destination", ShortSwitch = 'o', RequireParam = true,  AllowMultiple = false, },
            // リッピングしたファイルの出力先ディレクトリ
            new CommandLineOptionDefinition(){ LongSwitch = "mkdir",                          RequireParam = false, AllowMultiple = false, },
            // リッピング時のパラメータ
            new CommandLineOptionDefinition(){ LongSwitch = "format",      ShortSwitch = 'f', RequireParam = true, AllowMultiple = false, },
            new CommandLineOptionDefinition(){ LongSwitch = "samplerate",  ShortSwitch = 's', RequireParam = true, AllowMultiple = false, },
            new CommandLineOptionDefinition(){ LongSwitch = "bitrate",     ShortSwitch = 'b', RequireParam = true, AllowMultiple = false, },
            // リッピング対象のアルバム名
            new CommandLineOptionDefinition(){ LongSwitch = "albumname", RequireParam = true, AllowMultiple = false, },
            // アルバムのアーティスト名. 複数回指定した場合はトラック毎のアーティスト名
            new CommandLineOptionDefinition(){ LongSwitch = "artist",    RequireParam = true, AllowMultiple = true, },
            // アートワーク画像
            new CommandLineOptionDefinition(){ LongSwitch = "albumart",  RequireParam = true, AllowMultiple = false, },
            // リッピング対象のトラック数だけ指定するパラメータ
            new CommandLineOptionDefinition(){ LongSwitch = "track",     RequireParam = true, AllowMultiple = true, },
            new CommandLineOptionDefinition(){ LongSwitch = "title",     RequireParam = true, AllowMultiple = true, },
            new CommandLineOptionDefinition(){ LongSwitch = "filename",  RequireParam = true, AllowMultiple = true, },
            // リッピング対象のトラック数だけ指定するパラメータ
            // 1回だけ指定した場合は全曲に適用する
            new CommandLineOptionDefinition(){ LongSwitch = "genre",     RequireParam = true, AllowMultiple = true, },
            new CommandLineOptionDefinition(){ LongSwitch = "copyright", RequireParam = true, AllowMultiple = true, },
            new CommandLineOptionDefinition(){ LongSwitch = "comment",   RequireParam = true, AllowMultiple = true, },
            new CommandLineOptionDefinition(){ LongSwitch = "year",      RequireParam = true, AllowMultiple = true, },
        };

        /// <summary>サンプリングレートの列挙子</summary>
        private static Dictionary<string, Samplerate> SAMPLE_RATE_ARGS = new Dictionary<string, Samplerate>()
        {
            { "8000", Samplerate.asamples8000 },
            { "11025", Samplerate.bsamples11025 },
            { "22050", Samplerate.csamples22050 },
            { "44100", Samplerate.esamples44100 },
            { "48000", Samplerate.fsamples48000 },
        };

        /// <summary>ビットレートの列挙子</summary>
        private static Dictionary<string, Bitrate> BIT_RATE_ARGS = new Dictionary<string, Bitrate>()
        {
            {   "8", Bitrate.bitrate8 },
            {  "16", Bitrate.bitrate16 },
            {  "24", Bitrate.bitrate24 },
            {  "32", Bitrate.bitrate32 },
            {  "48", Bitrate.bitrate48 },
            {  "56", Bitrate.bitrate56 },
            {  "64", Bitrate.bitrate64 },
            {  "80", Bitrate.bitrate80 },
            {  "96", Bitrate.bitrate96 },
            { "112", Bitrate.bitrate112 },
            { "128", Bitrate.bitrate128 },
            { "144", Bitrate.bitrate144 },
            { "160", Bitrate.bitrate160 },
            { "176", Bitrate.bitrate176 },
            { "192", Bitrate.bitrate192 },
            { "224", Bitrate.bitrate224 },
            { "256", Bitrate.bitrate256 },
            { "320", Bitrate.bitrate320 },
        };

        public static void Main(string[] args)
        {
#if DEBUG
            ILogger logger = LoggerFactory.CreateLogger(LoggerName.ConsoleLogger);
#else
            ILogger logger = LoggerFactory.CreateLogger(LoggerName.NullLogger);
#endif
            var switches = CommandLineUtils.Parse(args, CMD_ARGS, true);
            if(switches.Options.ContainsKey("usage") || switches.Options.ContainsKey("help"))
            {
                // コマンドラインヘルプを表示して終了
                Console.Out.WriteLine(ShowUsage());
                return;
            }

            var conf = ParseArgs(switches);
            var ripper = new RipperMain()
            {
                Logger = logger,
                Conf = conf,
            }.Init();

            if (switches.Options.ContainsKey("devices"))
            {
                // PCに接続されている光学ドライブを列挙する
                ripper.EnumDevices(Console.Out);
                return;
            }
            else if (switches.Options.ContainsKey("open"))
            {
                // CDドライブを開く
                ripper.OpenCDDoor();
                return;
            }
            else if (switches.Options.ContainsKey("close"))
            {
                // CDドライブを閉じる
                ripper.CloseCDDoor();
                return;
            }
            else if (switches.Options.ContainsKey("formats"))
            {
                // サポートしているエンコーディングを返す
                ripper.EnumFormats(Console.Out);
                return;
            }
            else if (switches.Options.ContainsKey("info"))
            {
                // 挿入されているCDのトラック情報を返す
                ripper.EnumTrackInfo(Console.Out);
                return;
            }

            try
            {
                // リッピング開始
                ripper.Start(Console.Out);
            }
            catch(Exception eUnknown)
            {
                Console.Error.WriteLine($"[ERROR] {eUnknown.GetType().FullName}: {eUnknown.Message}");
                Console.Error.WriteLine(eUnknown.StackTrace);
            }
            finally
            {

            }

            return;
        }

        /// <summary>
        /// コマンドライン引数で渡したスイッチをアプリケーションの設定に格納しなおす
        /// </summary>
        /// <param name="opts"></param>
        /// <returns></returns>
        private static RipperConfig ParseArgs(CommandLineOptions opts)
        {
            var result = new RipperConfig();

            result.TargetDrive = (opts.Options.ContainsKey("drive") ? opts.Options["drive"].First() : "");

            // リッピング情報のテスト
            result.TestRippingPlan = opts.Options.ContainsKey("test");

            // リッピングしたファイルの出力先ディレクトリ
            result.DestinationDir = (
                opts.Options.ContainsKey("destination") ? new DirectoryInfo(opts.Options["destination"].First()) :
                null);

            // 出力先ディレクトリが存在しない場合にディレクトリの作成を許可するか？
            result.AllowCreateDirectory = opts.Options.ContainsKey("mkdir");

            // アルバム名
            result.AlbumName = (opts.Options.ContainsKey("albumname") ? opts.Options["albumname"].First() : "");

            // アルバムアート
            // 存在するファイルでかつ、jpeg、pngのいずれかでないとエラー
            if (opts.Options.ContainsKey("albumart"))
            {
                var options = opts.Options["albumart"];
                if (1 < options.Count())
                { throw new ArgumentException($"Switch \"albumart\" can only set once."); }

                var path = new FileInfo(options.First());

                if (!path.Exists || File.GetAttributes(path.FullName).HasFlag(FileAttributes.Directory))
                { throw new ArgumentException($"Invalid command line argument \"{options.First()}\" for switch \"albumart\". This switch can specify only exists file path."); }

                if (path.Extension.ToLower() != ".png" && path.Extension.ToLower() != ".jpg" && path.Extension.ToLower() != ".jpeg")
                { throw new ArgumentException($"Invalid command line argument \"{options.First()}\" for switch \"albumart\". This switch can specify only jpg, jpeg or png file."); }

                result.AlbumArtPath = path.FullName;
            }

            // トラック毎のリッピング情報
            if (opts.Options.ContainsKey("artist"))
            {
                foreach(var artist in opts.Options["artist"])
                { result.Artists.Add(artist); }
            }
            if (opts.Options.ContainsKey("track"))
            {
                foreach (var tr in opts.Options["track"])
                {
                    // トラック番号は1以上の数値として解釈できない場合はエラー
                    int trackNumber;
                    if (!int.TryParse(tr, out trackNumber) || trackNumber < 1)
                    { throw new ArgumentException($"Invalid command line argument \"{tr}\" for switch \"track\""); }

                    result.Tracks.Add(trackNumber);
                }
            }
            if (opts.Options.ContainsKey("title"))
            {
                foreach (var title in opts.Options["title"])
                { result.Titles.Add(title); }
            }
            if (opts.Options.ContainsKey("genre"))
            {
                foreach (var genre in opts.Options["genre"])
                { result.Genres.Add(genre); }
            }
            if (opts.Options.ContainsKey("copyright"))
            {
                foreach (var copyright in opts.Options["copyright"])
                { result.Copyrights.Add(copyright); }
            }
            if (opts.Options.ContainsKey("comment"))
            {
                foreach (var comment in opts.Options["comment"])
                { result.Comments.Add(comment); }
            }
            if (opts.Options.ContainsKey("year"))
            {
                foreach (var year in opts.Options["year"])
                {
                    // 収録年は数値として解釈可能、かつその範囲が [1900, 2099] の場合のみ許可
                    int y;
                    if (int.TryParse(year, out y) && 1900 <= y && y <= 2099)
                    { result.Years.Add(y); }
                    else
                    { throw new ArgumentException($"Invalid command line argument \"{year}\" for switch \"year\". year must set number between 1900 and 2099."); }
                }
            }

            if (opts.Options.ContainsKey("filename"))
            {
                foreach (var filename in opts.Options["filename"])
                { result.FileNames.Add(filename); }
            }

            // リッピング時のパラメータ
            if (opts.Options.ContainsKey("format"))
            {
                Format fmt;
                var formatName = opts.Options["format"].First();
                if (!Enum.TryParse<Format>(formatName.ToUpper(), out fmt))
                { throw new ArgumentException($"Invalid command line argument \"{formatName}\" for switch \"format\""); }
                else
                { result.FileFormat = fmt; }
            }
            else
            { result.FileFormat = Format.MP3; }

            if (opts.Options.ContainsKey("samplerate"))
            {
                var sampleRateName = opts.Options["samplerate"].First();
                if (!SAMPLE_RATE_ARGS.ContainsKey(sampleRateName))
                { throw new ArgumentException($"Invalid command line argument \"{sampleRateName}\" for switch \"samplerate\""); }
                else
                { result.SampleRate = SAMPLE_RATE_ARGS[sampleRateName]; }
            }
            else
            { result.SampleRate = Samplerate.esamples44100; }

            if (opts.Options.ContainsKey("bitrate"))
            {
                var bitRateName = opts.Options["bitrate"].First();
                if (!BIT_RATE_ARGS.ContainsKey(bitRateName))
                { throw new ArgumentException($"Invalid command line argument \"{bitRateName}\" for switch \"bitrate\""); }
                else
                { result.Bitrate = BIT_RATE_ARGS[bitRateName]; }
            }
            else
            { result.Bitrate = Bitrate.bitrate320; }

            return result;
        }

        /// <summary>
        /// コマンドラインヘルプをリソースから読み込んでその内容を返す
        /// </summary>
        /// <returns></returns>
        private static string ShowUsage()
        {
            var asm = Assembly.GetExecutingAssembly();
            using (var sr = new StreamReader(asm.GetManifestResourceStream(USAGE_FILE_NAME)))
            {
                var text = sr.ReadToEnd();
                return text;
            }
        }
    }
}
