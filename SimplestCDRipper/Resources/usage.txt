﻿Usage:

  --drive <drive letter>
    Set drive letter of CD drive to use ripping.
    drive letter *not* add colon. (example: set Q to using drive Q:)
    (default: first drive that enumerate devices)

  --bitrate <rate>
    Set encoding bit rate to <rate>
    values: 8 | 16 | 32 | 48 | 56 | 64 | 80 | 96 | 112 | 128 | 144 | 160 | 176 | 192 | 224 | 256 | 320
    (default: 320)

  --samplerate <rate>
    Set encoding sampling rate to <rate>.
    values: 8000 | 11025 | 22050 | 44100 | 48000
    (default: 44100)

  --format <Format>
    Set encoding format.
    values: AAC | MP2 | MP3 | OGG | WMA
    (default: MP3)

  --destination <directory>
    Set directory to output ripped audio file to <directory>.
    (default: current directory)

  --mkdir
    allow create directory when directory specified by --destination switch is not found.

  --track <track number>
    Set track number to ripping that is starting with 1.
    (default: Rip all tracks)

  --albumname <name>
    Set <name> to ID3 tag of album name on ripping audio file.
    (default: empty string)

  --albumart <file path>
    Set <file path> to image file to attach in MP3 file.
    This switch is only valid for mp3 file.
    <file path> can specify only PNG or JPEG file.
    (default: not specify)

  --artist <name>
    Set <name> to ID3 tag of artist name on ripping audio file.
    (default: empty string)

  --title <title>
    Set <title> to ID3 tag of title on ripping audio file.
    (default: "Track <track number>")

  --genre <genre>
    Set <genre> to ID3 tag of genre of music on ripping audio file.
    (default: not specify)

  --copyright <copyright>
    Set <copyright> to ID3 tag of copyright string on ripping audio file.
    (default: not specify)

  --comment <comment>
    Set <comment> to ID3 tag of comment on ripping audio file.
    (default: empty string)

  --year <year>
    Set <year> to ID3 tag of recording year on ripping audio file.
    <year> can specify only 4-digit number and between 1900 and 2099.
    (default: not specify)

  --filename <name>
    Set filename to output audio file name.
    *NOT* include file extension in this parameter
    (example: set "My Favorite Music", "My Favorite Music.mp3" is wrong)
    (default: "Track <track number>")

  --formats
    Show supported encodings by CSAudioCDRipper

  --devices
    Show optical device name and drive letter on this device.

  --open
    Open CD Door.

  --close
    Close CD Door.

  --info
    Show tracks info about inserted CD.

  --test
    Show rippng plan from arguments. not execute ripping.

  --help or --usage
    Show this help.
